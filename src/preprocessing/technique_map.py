import json
from collections import OrderedDict
from functools import cache
from types import MappingProxyType

import config


@cache
def get_technique_map():
    """
    Returns the technique map as an OrderedDict to preserver the original order
    from the json file.
    """

    with open(config.DS_BASE / 'technique_map.json') as f:
        return MappingProxyType(json.load(f, object_pairs_hook=OrderedDict))  # make dict immutable before returning


def lookup_technique(technique: str) -> str | None:
    """
    Lookup a raw technique (as found in the label files of the original datasets) in
    the technique map.

    :param technique: The raw technique to lookup
    :return: The technique name to use instead of the raw technique according to the
     technique map or `None` if the teechnique should be discarded from the dataset
     before training.
    """

    technique_map = get_technique_map()

    assert technique in technique_map, (f"Technique '{technique}' not found in technique map, thus it is unspecified "
                                        f"how to handle it. Please define it in data/technique_map.json.")

    if isinstance(technique_map.get(technique), str):
        return technique_map[technique]

    if technique_map.get(technique) is True:
        return technique

    return None


@cache
def get_all_techniques() -> tuple[str, ...]:
    """
    Get a list of all existing techniques, in a deterministic order.

    Note that the returned techniques are not "raw" techniques – i.e. the list of
    techniques returned in this function corresponds to the techniques returned
    by `lookup_technique()`.
    """

    res = []

    for k, v in get_technique_map().items():
        if v is True and k not in res:
            res.append(k)
        elif isinstance(v, str) and v not in res:
            res.append(v)

    return tuple(res)
