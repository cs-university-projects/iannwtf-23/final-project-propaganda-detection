import csv
import itertools
import logging
from pathlib import Path
from typing import Generator, Callable, Any, Literal

import pandas
import pandas as pd

import config
from src.preprocessing.technique_map import lookup_technique


def load_as_pandas_dataframe(
    root: str | Path,
    load_span_content: bool | Literal['full_article'] = False,
    span_extractor: Callable[[str, int, int], Any] | None = None
) -> pandas.DataFrame:
    return pandas.DataFrame(
        iter_classified_spans(root, load_span_content=load_span_content, span_extractor=span_extractor),
        columns=['article_id', 'start', 'end', 'text' if load_span_content else 'article_file', 'technique'],
    )


def iter_classified_spans(
        root: str | Path,
        load_span_content: bool | Literal['full_article'] = False,
        span_extractor: Callable[[str, int, int], Any] | None = None
) -> Generator[tuple[str, int, int, str | Path, str], None, None]:
    """
    Iterate through all annotated text spans in the dataset folder `root`.

    This function already takes care of looking up the raw techniques in the technique
    map and replaces techniques or discards spans accordingly.

    :param root: The dataset folder
    :param load_span_content: Whether to load content for each text span. This requires opening
      the according article file for each article and thus should only be done if the text span
      content is really needed, which, for calculating many statistics, is often not the case.
      The amount of files that need to be opened is roughly doubled if this option is turned on.
    :param span_extractor: A callback to extract the text of each span. The callback receives three
      arguments: the content of the whole article if `load_span_content` and the article file path otherwise,
      and the start and end of the current span as integers.
    :return: An iterator that yields a tuple for each annotated span in the dataset. The tuple has
      these elements:
       - the article id (str)
       - the start offset of the span in the article (int)
       - the end offset of the span in the article (int)
       - if `load_span_content` is `True`, the spans text content (str), otherwise the article file path (pathlib.Path)
       - the associated label (propaganda technique name) (str)
    """
    root = Path(root)
    assert root.is_dir(), "Dataset root not found or is not a directory"

    if load_span_content == 'full_article' and not span_extractor:
        span_extractor = (lambda text, s, e: text)
    span_extractor = span_extractor or (lambda text, s, e: text if isinstance(text, Path) else text[s:e])

    for file in root.glob("article*.labels.tsv"):
        with open(file) as labels_file:
            rows = sorted(csv.reader(labels_file, delimiter="\t"), key=lambda x: x[0])

        rows = [(aid, lookup_technique(t), s, e) for aid, t, s, e in rows]  # lookup raw techniques in technique map
        rows = [row for row in rows if row[1] is not None]  # filter out discarded techniques

        for article_id, spans in itertools.groupby(rows, key=lambda x: x[0]):
            content = None

            if load_span_content:
                with open(root / f'article{article_id}.txt', 'r') as article_file:
                    content = article_file.read()

            for _, t, start, end in spans:
                start, end = int(start), int(end)
                extracted_span = span_extractor(content or root / f'article{article_id}.txt', start, end)

                if not extracted_span:
                    logging.warning(f'Invalid (empty or out of bounds) span found in article {article_id}: {start}-{end} (labels file: {file}). Skipping.')
                    continue

                yield (
                    article_id,
                    start,
                    end,
                    extracted_span,
                    t
                )


def print_dataset_statistics(root: Path | str):
    df = load_as_pandas_dataframe(root, load_span_content=False)

    t = df['technique'].value_counts()
    d = pd.DataFrame.from_dict({
        'technique': t.index,
        'count': t.values,
        'proportion (%)': (t.values / t.sum() * 100).round(2)
    })

    print("Technique distribution:")
    print(d)
    print(f"Total count: {t.sum()}")


def iter_all_texts():
    for ds_root in [config.DS_DEV_ROOT, config.DS_TRAIN_ROOT]:
        for filename in sorted(ds_root.glob('article*.txt')):
            yield filename.read_text()


if __name__ == '__main__':
    print_dataset_statistics(config.DS_TRAIN_ROOT)
