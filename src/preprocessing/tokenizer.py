from typing import List, Tuple, Generator, Iterable

import tensorflow as tf
from nltk import RegexpTokenizer
from tensorflow_text import TokenizerWithOffsets


class TFTreebankTokenizerWithOffsets(tf.keras.preprocessing.text.Tokenizer):
    def __init__(self, *args, **kwargs):
        assert 'analyzer' not in kwargs
        assert not kwargs.get('char_level'), f'{self.__name__} does not support character-level tokenization.'
        self.base_tokenizer = BaseTokenizerWithOffsets()
        super().__init__(
            *args,
            **kwargs,
            analyzer=self.base_tokenizer.tokenize
        )
        self.__tokens_to_replace = {  # tokens to replace by others
            # IMPORTANT: only add same-length replacements to not screw up offsets!
            ord('’'): "'",
            ord('‘'): "'",
            ord('“'): '"',
            ord('”'): '"',
        }

    def fit_on_texts(self, texts: Iterable[str]):
        super().fit_on_texts(self.clean_text(t) for t in texts)

    def texts_to_sequences(self, texts: Iterable[str]) -> List[List[int]]:
        return [seq for seq, _, _, in self.texts_to_sequences_with_offsets_generator(texts)]

    def texts_to_sequences_generator(self, texts: Iterable[str]) -> Generator[List[int], None, None]:
        return (seq for seq, _, _, in self.texts_to_sequences_with_offsets_generator(texts))

    def texts_to_sequences_with_offsets_generator(self, texts: Iterable[str]) -> Generator[Tuple[List[int], List[int], List[int]], None, None]:
        num_words = self.num_words
        oov_token_index = self.word_index.get(self.oov_token)
        for text in texts:
            text = self.clean_text(text)
            seq, starts, ends = self.base_tokenizer.tokenize_with_offsets(text)
            vect = []
            for w in seq:
                i = self.word_index.get(w)
                if i is not None and not (num_words and i >= num_words):
                    vect.append(i)
                else:
                    i = self.word_index.get(self.clean_token(w))
                    if i is not None and not (num_words and i >= num_words):
                        vect.append(i)
                    elif self.oov_token is not None:
                        vect.append(oov_token_index)
            yield vect, starts, ends

    def texts_to_sequences_with_offsets(self, texts: Iterable[str]) -> List[Tuple[List[int], List[int], List[int]]]:
        return list(self.texts_to_sequences_with_offsets_generator(texts))

    def clean_text(self, input: str):
        """
        Applied to the whole text before tokenization takes place.
        """
        return (
            input
            .lower()
            .translate(self.__tokens_to_replace)
        )

    def clean_token(self, token: str):
        """
        Applied to tokens that couldn't be found in the word index to give them another
        shot at being found.
        """
        return token.strip("'.“”")


class BaseTokenizerWithOffsets(TokenizerWithOffsets):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # as tokens, we allow...
        self.treebank_tokenizer = RegexpTokenizer(r"\w+"         # ... words
                                                  r"|'s"         # ... "'s"
                                                  r"|\.\.\."     # ... "..."
                                                  r"|[!?]{2,3}"  # ... "???", "?!", "!!", etc.
                                                  r"|[^\w\s]")   # ... any other single-char punctuation or whitespace

    def tokenize_with_offsets(self, input: str) -> Tuple[List[str], List[int], List[int]]:
        spans = list(self.treebank_tokenizer.span_tokenize(input))
        tokens = [input[s:e] for s, e in spans]
        starts, ends = tuple(zip(*spans))
        return tokens, starts, ends

    def tokenize(self, input: str) -> List[str]:
        return self.treebank_tokenizer.tokenize(input)

