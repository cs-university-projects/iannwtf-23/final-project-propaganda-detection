from functools import cached_property
from typing import Callable, Tuple, List

import keras_tuner
import matplotlib.pyplot as plt
import numpy as np
import pandas
import sklearn.metrics
import tensorflow as tf
from sklearn.metrics import classification_report, roc_curve
from tensorflow.keras import layers, optimizers, losses, callbacks
from tensorflow.python.data.ops.dataset_ops import AUTOTUNE

import config
from src.base_model_manager import BaseModelManager
from src.glove_embedding import GloveEmbeddingLayer, text_to_padded_sequence, PADDING_TYPE, \
    TRUNCATION_TYPE, create_tokenizer
from src.preprocessing.load_data import load_as_pandas_dataframe
from src.span_identification_st.preprocessing import load_span_identification_st_dataset, chunk_text
from src.span_identification_st.utils import print_identified_spans, PerClassF1Score, merge_consecutive_spans
from src.utils import plot_training_history_graphs, plot_roc


class SpanIdentifierSequenceTagging(BaseModelManager):
    """
    Identify text span based on a sequence tagging approach.
    """

    hyperparameter_tuning_objective_metric = keras_tuner.Objective("val_f1_propaganda", direction="max")

    def _make_model(self, hp: keras_tuner.HyperParameters | None = None) -> tf.keras.Model:
        if hp:
            learning_rate = hp.Choice('learning_rate', values=[1e-2, 1e-3, 3e-3, 1e-4])
            gru_1_units = hp.Int('gru_1_units', min_value=32, max_value=256, step=16)
            gru_2_units = hp.Int('gru_2_units', min_value=4, max_value=32, step=4)
            dropout_1 = hp.Choice('dropout_1', values=[0.1, 0.2, 0.3, 0.4, 0.5])
            dropout_2 = hp.Choice('dropout_2', values=[0.1, 0.2, 0.3, 0.4, 0.5])
        else:
            learning_rate = config.SI_ST_LEARNING_RATE
            gru_1_units = 160
            gru_2_units = 24
            dropout_1 = 0.1
            dropout_2 = 0.2

        model = tf.keras.Sequential([
            tf.keras.Input((config.SI_ST_SEQUENCE_LENGTH,)),
            # GloVe embedding layer:
            GloveEmbeddingLayer(mask_zero=False, trainable=True),

            # Approach 1: CNN
            # layers.Conv1D(8, 3, activation='relu', padding='same'),
            # layers.Conv1D(8, 3, activation='relu', padding='same'),
            # layers.Conv1D(16, 3, activation='relu', padding='same'),
            # layers.Conv1D(16, 3, activation='relu', padding='same'),
            # layers.Dropout(0.2),

            # Approach 2: biLSTM:
            layers.Bidirectional(layers.GRU(gru_1_units, return_sequences=True, activation='tanh',
                                            kernel_regularizer=tf.keras.regularizers.L1(0.001))),
            layers.Dropout(dropout_1),
            layers.Bidirectional(layers.GRU(gru_2_units, return_sequences=True, activation='relu')),
            layers.Dropout(dropout_2),

            # Final classification layer:
            layers.TimeDistributed(layers.Dense(1, activation='sigmoid',
                                                kernel_regularizer=tf.keras.regularizers.L2(0.001)))
        ])

        model.compile(
            loss=losses.CategoricalFocalCrossentropy(),  # losses.MeanSquaredError(),
            optimizer=optimizers.Adam(learning_rate=learning_rate),
            metrics=[
                PerClassF1Score(0, name='f1_non-propaganda'),
                PerClassF1Score(1, name='f1_propaganda'),
                tf.keras.metrics.AUC(name='auc'),
            ]
        )

        return model

    def train(self, epochs: int = 15, plot: Callable[[callbacks.History], None] | bool = True):
        train_ds, val_ds = self.datasets

        history = self.model.fit(
            train_ds,
            epochs=epochs,
            steps_per_epoch=config.SI_ST_STEPS_PER_EPOCH,
            validation_data=val_ds,
            callbacks=[
                # callbacks.EarlyStopping(patience=7),
                callbacks.TensorBoard(log_dir=config.TENSORBOARD_LOG_DIR / 'si-st'),
            ],
        )

        print("Training finished.")
        print(self.model.summary())

        self.evaluate()

        if callable(plot):
            plot(history)
        elif plot:
            plt.figure(figsize=(4, 12), layout='constrained')
            plt.subplot(3, 1, 1)
            plot_training_history_graphs(history, 'f1_non-propaganda')
            plt.subplot(3, 1, 2)
            plot_training_history_graphs(history, 'f1_propaganda')
            plt.subplot(3, 1, 3)
            plot_training_history_graphs(history, 'loss')
            plt.show()

    def predict(self, text: str, merge_conscutive_spans: bool = True) -> List[Tuple[int, int, float]]:
        """
        Predict which spans in the given text are likely to contain propaganda.
        :param text: The text to analyze
        :param merge_conscutive_spans: Whether to merge consecutive propaganda-positive
         tokens into longer spans (i.e. phrases) or return individual tokens.
        :return:
        """
        all_preds, all_token_spans = [], []
        for _, end, tokens, token_spans in chunk_text(text, create_tokenizer(), config.SI_ST_SEQUENCE_LENGTH):
            seq = np.array(tokens).reshape((1, -1))
            preds = tuple(float(e) for e in self.model(seq)[0])
            all_preds.extend(preds)
            all_token_spans.extend(token_spans)
        starts, ends = list(zip(*all_token_spans))
        if merge_conscutive_spans:
            return merge_consecutive_spans(starts, ends, all_preds)
        else:
            return list(zip(starts, ends, all_preds))

    def evaluate(self):
        _, val_ds = self.datasets

        # Evaluate model:
        self.model.evaluate(val_ds)

        # Generate and print a classification report for the test dataset:
        x, y_test = tuple(zip(*val_ds.unbatch()))
        y_pred = self.model.predict(np.vstack(np.array(x))).flatten()
        y_test = np.array(y_test).flatten()
        print(classification_report(y_test, y_pred.round(),  # round model output to either 1 or 0
                                    target_names=['non-propaganda', 'propaganda']))

        # Compute AUC score:
        auc = sklearn.metrics.roc_auc_score(y_test, y_pred)
        print(f"AUC Score: {auc}")

        # Plot the ROC curve:
        fpr, tpr, thresholds = roc_curve(np.array(y_test).flatten(), y_pred)
        plot_roc(fpr, tpr)

    @cached_property
    def datasets(self):
        print("Loading datasets...")
        train_df, val_df = self.dataframes

        train_ds = self.__create_tf_dataset(train_df)
        val_ds = self.__create_tf_dataset(val_df)

        return train_ds, val_ds

    @cached_property
    def dataframes(self):
        train_df = load_span_identification_st_dataset(config.DS_TRAIN_ROOT)
        val_df = load_span_identification_st_dataset(config.DS_TEST_ROOT)
        return train_df, val_df

    @staticmethod
    def __create_tf_dataset(dataframe: pandas.DataFrame):
        dataframe['sequence'] = dataframe['sequence'].map(lambda text: text_to_padded_sequence(text, config.SI_ST_SEQUENCE_LENGTH))
        dataframe['label_mask'] = dataframe['label_mask'].map(lambda m: tf.keras.utils.pad_sequences(
            [m], maxlen=config.SI_ST_SEQUENCE_LENGTH,
            padding=PADDING_TYPE, truncating=TRUNCATION_TYPE,
        )[0])
        ds = tf.data.Dataset.from_tensor_slices(dataframe[['sequence', 'label_mask']].to_dict(orient='list'))
        ds = ds.map(lambda x: (x['sequence'], x['label_mask']))
        ds = ds.shuffle(250)
        ds = ds.batch(config.SI_ST_BATCH_SIZE, drop_remainder=True)
        ds = ds.prefetch(AUTOTUNE)
        ds = ds.repeat(3)
        return ds


if __name__ == '__main__':
    si = SpanIdentifierSequenceTagging()
    si.load_weights()
    si.evaluate()

    # Predict and pretty-print some random examples from the test ds:
    tokenizer = create_tokenizer()
    df = load_span_identification_st_dataset(
        lambda: load_as_pandas_dataframe(config.DS_TEST_ROOT).sort_values('article_id').head(100),
    ).take([2, 3, 6, 8, 11])
    df['sequence'] = df['sequence'].map(lambda s: text_to_padded_sequence(s, config.SI_ST_SEQUENCE_LENGTH))

    for idx in df.index:
        content, sequence, label_mask = df['content'][idx], df['sequence'][idx], df['label_mask'][idx]
        start, end = df['start'][idx], df['end'][idx]

        res = si.model(np.array(sequence).reshape((1, -1)))

        print(f"\n\nArticle: {df['article_id'][idx]}")
        print_identified_spans(
            content,
            [(s[0] - start, s[1] - start) for s in df['token_spans'][idx]],
            sequence,
            label_mask,
            res[0],
        )

    # Print classification report:
    val_ds = si.datasets[1]
    x, y_test = tuple(zip(*val_ds.unbatch()))
    y_pred = si.model.predict(np.vstack(np.array(x))).round()  # round model output to either 1 or 0
    print(classification_report(np.array(y_test).flatten(), y_pred.flatten(), target_names=['non-propaganda', 'propaganda']))

