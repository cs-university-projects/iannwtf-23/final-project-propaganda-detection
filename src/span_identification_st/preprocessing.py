from pathlib import Path
from typing import Tuple, List, Generator, Sequence, Callable

import nltk
import numpy as np
import pandas

import config
from src.glove_embedding import create_tokenizer
from src.preprocessing.load_data import load_as_pandas_dataframe
from src.preprocessing.tokenizer import TFTreebankTokenizerWithOffsets


def load_span_identification_st_dataset(
    root: str | Path | Callable[[], pandas.DataFrame],
    augmentations: int = 0,
) -> pandas.DataFrame:
    """
    Returns a pandas.DataFrame with the following columns:
     - article_id: str - the unique article id
     - start: int - the start offset of the chunk in the article
     - end: int - the end offset of the chunk in the article
     - content: str - the content of the chunk of the article
     - token_spans: List[Tuple[int, int]] - the start and end offsets of each token in the article text
     - sequence: List[int] - the tokenized sequence of the current chunk (list of word indices of the tokenizer)
     - label_mask: numpy.ndarray of shape (config.SI_ST_SEQUENCE_LENGTH,) filled with zeroes and ones - whether each token in the
       chunk is classified as propaganda (1) or not (0)
    """

    all_spans = load_as_pandas_dataframe(root, load_span_content=False) if not callable(root) else root()
    tokenizer = create_tokenizer()

    def generator():
        for (article_id, article_file), spans in all_spans.groupby(by=['article_id', 'article_file']):
            with open(article_file) as f:
                content = f.read()

            prop_spans = tuple(spans[['start', 'end']].itertuples(index=False))

            for start, end, tokens, token_spans in chunk_text(content, tokenizer, config.SI_ST_SEQUENCE_LENGTH):
                label_mask = create_label_mask(token_spans, prop_spans)
                yield article_id, start, end, content[start:end], token_spans, tokens, label_mask

    return pandas.DataFrame(
        generator(),
        columns=["article_id", "start", "end", "content", "token_spans", "sequence", "label_mask"],
    )


def chunk_text(text: str, tokenizer: TFTreebankTokenizerWithOffsets, chunk_size: int) -> Generator[Tuple[int, int, List[int], List[Tuple[int, int]]], None, None]:
    """
    Convert the given text to chunks of chunk_size tokens. Does not break sentences,
    i.e. chunks will usually be shorter than the given `chunk_size`, but might also
    be longer in case a single sentence exceeds `chunk_size` tokens.

    This method yields a tuple containing of the following items for each chunk:
     - start_offset: int – the start offset of the chunk
     - end_offset: int – the end offset of the chunk
     - tokens: List[int] – the sequence of tokens comprising this chunk
     - token_spans: List[Tuple[int, int]] – the start and end offsets of each token in
       this chunk, relative to the given `text`, **not** to the chunk's `start_offset`.

    :param text: The text to split into chunks
    :param tokenizer: The tokenizer to use.
    :param chunk_size: The max. number of tokens for each chunk
    :return: A generator yielding each chunk one-by-one (see above for details)
    """

    sentence_spans = list(nltk.load('tokenizers/punkt/english.pickle').span_tokenize(text))

    tokenized_text, _s, _e = tokenizer.texts_to_sequences_with_offsets([text])[0]
    token_spans = list(zip(_s, _e))

    current_tokens: List[int] = []
    current_token_spans: List[Tuple[int, int]] = []
    current_sentence_spans: List[Tuple[int, int]] = []

    while sentence_spans:
        sentence_start, sentence_end = sentence_spans.pop(0)

        # Collect all tokens of this sentence, but at most `chunk_size` tokens:
        sentence_tokens = []
        sentence_token_spans = []
        while token_spans and token_spans[0][1] <= sentence_end:
            sentence_tokens.append(tokenized_text.pop(0))
            sentence_token_spans.append(token_spans.pop(0))

            # If we exceed `chunk_size` tokens in this sentence, create a new "fake" sentence
            # for the remaining tokens of this sentence, such that they'll effectively be
            # prepended to the next chunk:
            if len(sentence_tokens) == chunk_size:  # this usually doesn't happen, but could in theory
                sentence_end = sentence_token_spans[-1][1]
                sentence_spans.insert(0, (sentence_end, sentence_spans[0][0]))
                break

        # Determine whether the sentence should still be part of this chunk or of the next one:
        if len(current_tokens) + len(sentence_tokens) <= chunk_size:
            current_tokens += sentence_tokens
            current_token_spans += sentence_token_spans
            current_sentence_spans.append((sentence_start, sentence_end))
        else:
            yield current_sentence_spans[0][0], current_sentence_spans[-1][1], current_tokens, current_token_spans
            current_tokens = sentence_tokens
            current_token_spans = sentence_token_spans
            current_sentence_spans = [(sentence_start, sentence_end)]

    # Yield all remaining:
    if current_tokens:
        yield current_sentence_spans[0][0], current_sentence_spans[-1][1], current_tokens, current_token_spans


def create_label_mask(token_spans: Sequence[Tuple[int, int]], spans_to_mask: Sequence[Tuple[int, int]]) -> np.ndarray:
    """
    Creates a label mask for the given tokenized content. This mask is a 1-D numpy array of the
    same length as `token_spans`. The mask is filled with zeroes and contains ones at the index
     of each token that overlaps with the given `spans_to_mask`.
    :param token_spans: A list of `(start_offset, end_offset)` tuples for each token
    :param spans_to_mask: A list of `(start_offset, end_offset)` tuples for each span the
      should be masked
    :return: A 1-D numpy array of same length as `token_spans`
    """
    label_mask = np.zeros((len(token_spans),))
    for (prop_start, prop_end) in spans_to_mask:
        for ti, (ts, te) in enumerate(token_spans):
            if ts <= prop_end and te >= prop_start:  # token overlaps with propaganda span
                label_mask[ti] = 1
    return label_mask


# For testing, inspecting & debugging, run this:
if __name__ == '__main__':
    res = load_span_identification_st_dataset(config.DS_DEV_ROOT)

    tokenizer = create_tokenizer()
    rev_word_index = {v: k for k, v in tokenizer.word_index.items()}

    for idx in range(4):
        chunk_start, chunk_end = res['start'][idx], res['end'][idx]
        print(f"\nChunk #{idx} (article: {res['article_id'][idx]}, span: {chunk_start, chunk_end}):")
        l1 = []
        l2 = []
        l3 = []
        for token, (s, e), mask_value in zip(res['sequence'][idx], res['token_spans'][idx], res['label_mask'][idx]):
            rep1 = repr(res['content'][idx][s - chunk_start:e - chunk_start])
            rep2 = rev_word_index[token]
            l1.append(rep1.ljust(max(len(rep1), len(rep2)), ' '))
            l2.append(rep2.rjust(max(len(rep1), len(rep2)), ' '))
            l3.append(('–' if mask_value else ' ') * max(len(rep1), len(rep2)))
        print(', '.join(l1))
        print(', '.join(l2))
        print('  '.join(l3))

