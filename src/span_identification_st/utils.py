import math
from typing import Tuple, List, Sequence

import supports_color
import tensorflow as tf
import termcolor

from src.glove_embedding import create_tokenizer


def print_with_highlighted_spans(text: str, spans: List[Tuple[int, int, float]]):
    """
    Prints the given text while highlighting the given spans in different colors,
    according to their respective level of confidence (predicted probability).

    Uses colors, if supported by stdout.

    :param text: The text to print.
    :param spans: A list of tuples containing (start_offset, end_offset, confidence)
     each, where confidence is a float between 0 and 1.
    """

    coloring_enabled = termcolor.colored('test', 'red') != 'test'  # can be disabled e.g. by setting env var `NO_COLOR`
    color_levels = ['green', 'yellow', 'red', 'magenta']
    res = text
    offset = 0

    if not supports_color.supportsColor.stdout or not coloring_enabled:
        spans = merge_consecutive_spans(*zip(*spans))

    for s, e, prob in spans:
        if prob < 0.5:  # skip out non-propaganda spans
            continue

        original_span = res[s + offset:e + offset]
        if supports_color.supportsColor.stdout and coloring_enabled:
            color = color_levels[math.floor(len(color_levels) * (prob - 0.5) * 2)]
            highlighted_span = termcolor.colored(original_span, color)
        else:
            highlighted_span = f'**[{original_span}]**'

        res = res[:s + offset] + highlighted_span + res[e + offset:]
        offset += len(highlighted_span) - len(original_span)

    print(res)


def print_identified_spans(content, token_spans, tokens, label_mask, predictions=None):
    """
    Prints the content, tokens, label mask and (if given) the predictions in a nicely formatted
    way. This function is rather intended for internal usage, detailed inspection or
    debugging. For end-users, use the [print_with_highlighted_spans()] function instead.

    :param content: The raw text content
    :param token_spans: The spans for each token, as tuples (start_offset, end_offset)
    :param tokens: The token identifiers as returned by the tokenizer from [create_tokenizer()]
    :param label_mask: The label mask, as an iterable of zeros and ones, one for each token
    :param predictions: The predictions as an iterable of numbers between 0 and 1, one for each token
    """

    tokenizer = create_tokenizer()
    rev_word_index = {v: k for k, v in tokenizer.word_index.items()}

    l: List[Tuple[str, ...]] = []
    for i, (token, (s, e), mask_value) in enumerate(zip(tokens, token_spans, label_mask)):
        rep1 = repr(content[s:e])
        rep2 = rev_word_index[token]

        col = [
            rep1,
            rep2,
            '-' if mask_value else ' ',
        ]

        if predictions is not None:
            col.append(('*' if predictions[i] > 0.5 else ' '))
            col.append(str(round(float(predictions[i])*100, 1)))

        m = max(len(x) for x in col)

        l.append((
            col[0].ljust(m, ' '),
            col[1].rjust(m, ' '),
            col[2] * m,
            *((
                col[3] * m,
                col[4].rjust(m, ' '),
            ) if predictions is not None else ())
        ))

    lines = tuple(zip(*l))
    print('Content: ', ', '.join(lines[0]))
    print('Tokens:  ', ', '.join(lines[1]))
    print('Labels:  ', '  '.join(lines[2]))
    if predictions is not None:
        print('Pred.:   ', '  '.join(lines[3]))
        print('Prob. %: ', '  '.join(lines[4]))


class PerClassF1Score(tf.keras.metrics.Metric):
    """
    A custom tensorflow metric that computes the f1-score for a specific
    class in a result. This metric is only usable for non-one-hot-encoded
    model outputs and labels.
    """

    def __init__(self, class_: int, name: str | None = None, **kwargs):
        super().__init__(name=name or f'per_class_f1_{class_}', **kwargs)
        self.class_ = class_
        self.tp = self.add_weight(name='tp', initializer='zeros', dtype=tf.int64)
        self.fp = self.add_weight(name='fp', initializer='zeros', dtype=tf.int64)
        self.fn = self.add_weight(name='fn', initializer='zeros', dtype=tf.int64)

    def update_state(self, y_true, y_pred, sample_weight=None):
        assert sample_weight is None, 'sample_weight is not supported by PerClassF1Score'

        y_true, y_pred = _squeeze_or_expand_to_same_rank(y_true, y_pred)
        y_true = tf.equal(y_true, self.class_)
        y_pred = tf.equal(tf.math.round(y_pred), self.class_)

        true_p = tf.logical_and(tf.equal(y_true, True), tf.equal(y_pred, True))
        false_p = tf.logical_and(tf.equal(y_true, False), tf.equal(y_pred, True))
        false_n = tf.logical_and(tf.equal(y_true, True), tf.equal(y_pred, False))
        self.tp.assign_add(tf.reduce_sum(tf.cast(true_p, tf.int64)))
        self.fp.assign_add(tf.reduce_sum(tf.cast(false_p, tf.int64)))
        self.fn.assign_add(tf.reduce_sum(tf.cast(false_n, tf.int64)))

    def reset_state(self):
        self.tp.assign(0)
        self.fp.assign(0)
        self.fn.assign(0)

    def result(self):
        prec = self.tp / (self.tp + self.fp)
        rec = self.tp / (self.tp + self.fn)
        return 2 / (1/prec + 1/rec)


def _squeeze_or_expand_to_same_rank(x1, x2, expand_rank_1=True):
    """Squeeze/expand last dim if ranks differ from expected by exactly 1."""
    # Taken from: https://github.com/keras-team/keras/blob/master/keras/losses/loss.py
    # seems to not be included in tf.keras anymore, thus copied old source

    x1_rank = len(x1.shape)
    x2_rank = len(x2.shape)
    if x1_rank == x2_rank:
        return x1, x2
    if x1_rank == x2_rank + 1:
        if x1.shape[-1] == 1:
            if x2_rank == 1 and expand_rank_1:
                x2 = tf.keras.ops.expand_dims(x2, axis=-1)
            else:
                x1 = tf.keras.ops.squeeze(x1, axis=-1)
    if x2_rank == x1_rank + 1:
        if x2.shape[-1] == 1:
            if x1_rank == 1 and expand_rank_1:
                x1 = tf.keras.ops.expand_dims(x1, axis=-1)
            else:
                x2 = tf.keras.ops.squeeze(x2, axis=-1)
    return x1, x2


def merge_consecutive_spans(token_starts: Sequence[int],
                            token_ends: Sequence[int],
                            token_predictions: Sequence[float],
                            prediction_merger=max) -> List[Tuple[int, int, float]]:
    """Merges consecutive spans into longer spans, averaging th prediction values."""

    # zip the arguments, so they can easily be removed together (using list.pop below):
    preds = list(zip(
        token_predictions,
        token_starts,
        token_ends,
    ))
    res = []

    while preds:
        # Drop all negative tokens:
        while preds and preds[0][0] < 0.5:
            preds.pop(0)

        # initialize variables for this span:
        span = []

        # Collect whole span (all probabilities) until we reach a non-prop span again:
        while preds and preds[0][0] >= 0.5:
            span.append(preds.pop(0))

        if span:  # at least one token must exist, might not be satisfied at end of input
            res.append((
                span[0][1],
                span[-1][2],
                prediction_merger(tuple(p for p, _, _ in span))
            ))

    return res

