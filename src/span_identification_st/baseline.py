from pathlib import Path

import numpy as np
from sklearn.metrics import classification_report, roc_auc_score

import config
from src.span_identification_st import load_span_identification_st_dataset


def baseline_all_nonprop(ds_root: str | Path = config.DS_TEST_ROOT):
    """
    Baseline that classifies all tokens into non-propaganda.
    The baseline is performed on the validation dataset, and then,
    a classification report is printed.
    """

    val_df = load_span_identification_st_dataset(ds_root)

    y_test = np.concatenate(val_df['label_mask']).ravel()
    y_pred = np.zeros(y_test.shape)

    print(classification_report(y_test, y_pred, target_names=['non-propaganda', 'propaganda'], zero_division=np.nan))

    auc = roc_auc_score(y_test, y_pred)
    print(f"AUC Score: {auc}")


if __name__ == '__main__':
    baseline_all_nonprop()
