from functools import cached_property
from typing import Callable

import keras_tuner
import matplotlib.pyplot as plt
import numpy as np
import pandas
import sklearn
import tensorflow as tf
from keras.src.callbacks import History
from sklearn.metrics import classification_report, roc_curve
from tensorflow.keras import layers, optimizers, losses, metrics, callbacks
from tensorflow.python.data.ops.dataset_ops import AUTOTUNE

import config
from src.base_model_manager import BaseModelManager
from src.glove_embedding import GloveEmbeddingLayer, text_to_padded_sequence
from src.span_identification.preprocessing import load_span_identification_dataset
from src.utils import plot_training_history_graphs, plot_roc


class SpanIdentifier(BaseModelManager):
    """
    Binary text span classification.
    """

    hyperparameter_tuning_objective_metric = keras_tuner.Objective("val_accuracy", direction="max")

    def _make_model(self, hp: keras_tuner.HyperParameters | None = None) -> tf.keras.Model:
        if hp:
            learning_rate = hp.Choice('learning_rate', values=[1e-2, 1e-3, 3e-3, 1e-4])
            gru_units = hp.Int('gru_units', min_value=4, max_value=32, step=8)
            dropout = hp.Choice('dropout', values=[0.1, 0.2, 0.3, 0.4, 0.5])
        else:
            learning_rate = config.SI_LEARNING_RATE
            gru_units = 28
            dropout = 0.2

        model = tf.keras.Sequential([
            tf.keras.Input((config.SI_SEQUENCE_LENGTH,)),
            # GloVe embedding layer:
            GloveEmbeddingLayer(mask_zero=False),

            layers.Bidirectional(layers.GRU(gru_units, return_sequences=True)),
            layers.GlobalMaxPooling1D(),
            layers.Dropout(dropout),

            # Final classification layer:
            layers.Dense(1, activation='sigmoid')
        ])

        model.compile(
            loss=losses.BinaryCrossentropy(),
            optimizer=optimizers.Adam(learning_rate=learning_rate),
            metrics=[
                metrics.BinaryAccuracy(name="accuracy"),
            ]
        )

        return model

    def train(self, epochs: int = 6, plot: Callable[[History], None] | bool = True):
        train_ds, val_ds = self.datasets

        history = self.model.fit(
            train_ds,
            batch_size=config.SI_BATCH_SIZE,
            epochs=epochs,
            validation_data=val_ds,
            callbacks=[
                # callbacks.EarlyStopping(patience=7),
                callbacks.TensorBoard(log_dir=config.TENSORBOARD_LOG_DIR / 'st'),
            ],
        )

        print("Training finished.")
        print(self.model.summary())

        self.evaluate()

        if callable(plot):
            plot(history)
        elif plot:
            plt.figure(figsize=(5, 12))
            plt.subplot(2, 1, 1)
            plot_training_history_graphs(history, 'accuracy')
            plt.subplot(2, 1, 2)
            plot_training_history_graphs(history, 'loss')
            plt.show()

    def evaluate(self):
        _, val_ds = self.datasets

        x, y_test = tuple(zip(*val_ds.unbatch()))
        y_pred = self.model.predict(np.vstack(np.array(x)))
        print(classification_report(y_test, y_pred.round(),  # round model output to either 1 or 0
                                    target_names=['non-propaganda', 'propaganda']))

        # Compute AUC score:
        auc = sklearn.metrics.roc_auc_score(y_test, y_pred)
        print(f"AUC Score: {auc}")

        # Plot the ROC curve:
        fpr, tpr, thresholds = roc_curve(np.array(y_test).flatten(), y_pred)
        plot_roc(fpr, tpr)

    @cached_property
    def datasets(self):
        print("Loading datasets...")
        train_df, val_df = self.dataframes

        train_ds = self.__create_tf_dataset(train_df)
        val_ds = self.__create_tf_dataset(val_df)

        return train_ds, val_ds

    @cached_property
    def dataframes(self):
        train_df = load_span_identification_dataset(config.DS_TRAIN_ROOT)
        val_df = load_span_identification_dataset(config.DS_TEST_ROOT)
        return train_df, val_df

    @staticmethod
    def __create_tf_dataset(dataframe: pandas.DataFrame):
        dataframe['span'] = dataframe['span'].map(lambda text: text_to_padded_sequence(text, config.SI_SEQUENCE_LENGTH))
        ds = tf.data.Dataset.from_tensor_slices(dataframe[['span', 'label']].to_dict(orient='list'))
        ds = ds.map(lambda x: (x['span'], [x['label']]))
        ds = ds.shuffle(250)
        ds = ds.batch(config.SI_BATCH_SIZE, drop_remainder=True)
        ds = ds.prefetch(AUTOTUNE)
        return ds


if __name__ == '__main__':
    si = SpanIdentifier()
    si.load_weights()
    si.evaluate()
