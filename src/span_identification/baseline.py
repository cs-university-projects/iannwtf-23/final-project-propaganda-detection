import numpy as np
import sklearn
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report

import config
from src.span_identification import load_span_identification_dataset


def baseline_logistic_regression():
    """
    Fits a logistic regression model on the length of each span in
    the training dataset and prints a classification report on its
    predictions for the test dataset.
    """
    train_df = load_span_identification_dataset(config.DS_TRAIN_ROOT)
    val_df = load_span_identification_dataset(config.DS_TEST_ROOT)

    train_df['span_length'] = train_df['end'] - train_df['start']
    val_df['span_length'] = val_df['end'] - val_df['start']

    model = LogisticRegression(penalty='l2', class_weight='balanced', solver="lbfgs")
    model.fit(np.array(train_df['span_length']).reshape(-1, 1), np.array(train_df['label']))

    preds = model.predict(np.array(val_df['span_length']).reshape(-1, 1))

    print(classification_report(val_df['label'], preds, target_names=['non-propaganda', 'propaganda']))

    # Compute AUC score:
    auc = sklearn.metrics.roc_auc_score(val_df['label'], preds)
    print(f"AUC Score: {auc}")


if __name__ == '__main__':
    baseline_logistic_regression()
