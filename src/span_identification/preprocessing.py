import logging
import math
import random
import string
from pathlib import Path
from typing import Sequence, Tuple, Generator

import nltk
import pandas

import config
from src.preprocessing.load_data import load_as_pandas_dataframe


def load_span_identification_dataset(
    root: str | Path,
    non_propaganda_ratio: float = 1
) -> pandas.DataFrame:
    """
    Returns a pandas.DataFrame with the following columns:
     - article_id: str - the unique article id
     - start: int - the start offset of the span in the article
     - end: int - the end offset of the span in the article
     - span: str - the text content of the span
     - label: int (0 or 1) - whether the span is classified as propaganda (1) or not (0)
    """

    all_spans = load_as_pandas_dataframe(root, load_span_content=False)
    sentence_tokenizer: nltk.PunktSentenceTokenizer = nltk.load(f"tokenizers/punkt/english.pickle")

    def generator():
        for (article_id, article_file), spans in all_spans.groupby(by=['article_id', 'article_file']):
            with open(article_file) as f:
                content = f.read()

            prop_spans = tuple(spans[['start', 'end']].itertuples(index=False))

            # Yield propaganda spans:
            for (start, end) in prop_spans:
                if content[start:end].strip():
                    yield article_id, start, end, content[start:end], 1

            # Calculate the number of non-propaganda spans to generate for this article:
            num_nonprop_spans = len(prop_spans) * non_propaganda_ratio
            num_nonprop_spans = (
                math.floor(num_nonprop_spans)
                if random.random() < num_nonprop_spans % 1
                else math.ceil(num_nonprop_spans)
            )
            logging.debug(
                f"Generating {num_nonprop_spans} non-propaganda spans for {len(prop_spans)} propaganda spans.")

            gaps_between_spans = list(_find_gaps(prop_spans, length=len(content)))

            # Generate and yield non-propaganda spans of lengths close to those of the propaganda spans:
            ideal_lengths = [
                prop_spans[i % len(prop_spans)][1] - prop_spans[i % len(prop_spans)][0]
                for i in range(num_nonprop_spans)
            ]
            for start, end in _find_random_spans(ideal_lengths, content, gaps_between_spans, sentence_tokenizer):
                if content[start:end].strip():
                    yield article_id, start, end, content[start:end], 0

    return pandas.DataFrame(generator(), columns=["article_id", "start", "end", "span", "label"])


def _find_random_spans(ideal_lengths: Sequence[int], content: str, gaps_between_spans: Sequence[Tuple[int, int]], sentence_tokenizer) -> Tuple[int, int]:
    """
    Generator yielding a random span (start and end offsets) for each length in `ideal_lengths`, that
    aims to have a length close to the corresponding `ideal_length`. All yielded spans lie within one
    of the `gaps_between_spans` and do not start or end within a word, but only at a word boundary.
    :param ideal_lengths: A list of ideal span lengths to aim for
    :param content: The original text content, used to find word and sentence boundaries
    :param gaps_between_spans: The gaps between existing spans wherein the generated span should lie
    :param sentence_tokenizer:  An nltk.SentenceTokenizer used to find whole sentences in `content`
    :return:
    """
    gaps_between_spans = [g for g in gaps_between_spans if g[0] != g[1]]
    remaining_gaps = list(gaps_between_spans)

    if not gaps_between_spans:
        return

    for ideal_length in ideal_lengths:
        # Choose a gap to pick a span from:
        possible_gaps = [idx for idx, (s, e) in enumerate(remaining_gaps) if e - s >= ideal_length]
        if possible_gaps:
            gap_start, gap_end = remaining_gaps.pop(random.choice(possible_gaps))
        elif remaining_gaps:
            gap_start, gap_end = remaining_gaps.pop(random.randint(0, len(remaining_gaps) - 1))
        else:
            gap_start, gap_end = random.choice(gaps_between_spans)

        # Choose a sentence or a sequence of sentences from the gap, that is longer than `ideal_length`, if possible:
        sentences = tuple(sentence_tokenizer.span_tokenize(content[gap_start:gap_end]))
        possible_sentences = [idx for idx, (s, e) in enumerate(sentences) if e - s >= ideal_length]
        s = e = random.choice(possible_sentences or range(len(sentences)))
        while sentences[e][1] - sentences[s][0] < ideal_length:
            r = random.random()
            prob_left, prob_right = int(s > 0) * r, int(e < len(sentences) - 1) * (1 - r)
            if prob_left > prob_right:
                s -= 1
            elif prob_right > prob_left:
                e += 1
            elif prob_left == prob_right == 0:
                break

        # Select a span of whole words that is about `ideal_length` character long from the chosen sentence(s):
        span = content[sentences[s][0]:sentences[e][1]]
        shift = random.random()
        start = _find_closest_non_word_char_index(round(max(len(span) - ideal_length, 0) * shift), span)
        end = _find_closest_non_word_char_index(round(len(span) - max(len(span) - ideal_length, 0) * (1 - shift)), span)
        start = start + 1 if start else 0
        end = end if end else len(span) + 1
        logging.debug(f"_find_random_span: Ideal length={ideal_length}, actual length={len(span[start:end])}: \"{span[start:end]}\"")

        yield sentences[s][0] + start, sentences[s][0] + end


def _find_gaps(spans: Sequence[Tuple[int, int]], length: int, min_gap_length: int = 6) -> Generator[Tuple[int, int], None, None]:
    """
    Finds gaps between the given spans.
    """
    # Sort spans based on their start, then end points
    sorted_spans = sorted([(0, 0), *spans, (length, length)])

    # Iterate through sorted spans to find gaps
    for i in range(len(sorted_spans) - 1):
        start = sorted_spans[i][1]
        end = sorted_spans[i + 1][0]

        if end - start < min_gap_length:
            continue

        # If not span overlaps, return this gap
        if not any(s < end and e > start for s, e in sorted_spans):
            yield start, end

        # Note: This algorithm might miss some gaps, but this shouldn't be a problem
        # in most cases, since there'll usually still be enough gaps left.


def _find_closest_non_word_char_index(start_index: int, text: str) -> int | None:
    """
    Returns the index of the punctuation or whitespace character that is closest to `start_index`
    in `text`. If no such character is found before reaching the end of the string in both directions,
    `None` is returned.
    """
    chars = set(string.punctuation + string.whitespace) - {"'"}
    i = 1
    while start_index - i >= 0 or start_index + i < len(text):
        if start_index - i >= 0 and text[start_index - i].lower() in chars:
            return start_index - i
        if start_index + i < len(text) and text[start_index + i].lower() in chars:
            return start_index + i
        i += 1


# For testing, inspecting & debugging, run this:
if __name__ == '__main__':
    res = load_span_identification_dataset(config.DS_DEV_ROOT, non_propaganda_ratio=0.75)
    print(res.head(50))

    num_prop = res['label'].value_counts()[1]
    num_non_prop = len(res) - num_prop

    print(f"\nPropaganda spans: {num_prop}\n"
          f"Non-propaganda spans: {num_non_prop}\n"
          f"Resulting ratio (non_propaganda:propaganda): {num_non_prop / num_prop}\n")
