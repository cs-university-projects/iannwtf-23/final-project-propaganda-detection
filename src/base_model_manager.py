import abc
from pathlib import Path
from typing import Tuple

import keras_tuner
import tensorflow as tf

import config


class BaseModelManager(abc.ABC):
    """
    A base class providing utilities for managing (training, applying and hp-tuning)
    tensorflow models.
    """

    def __init__(self):
        self.model = self._make_model()

    @abc.abstractmethod
    def _make_model(self, hp: keras_tuner.HyperParameters | None = None) -> tf.keras.Model:
        """Return a compiled tf.keras.Model. If `hp` is provided, specify the model's
        hyperparameters using it."""
        pass

    @abc.abstractmethod
    def train(self, epochs: int):
        """Train the model for the specified number of epochs."""
        pass

    def save_weights(self, fn: str | None = None):
        """Save weights to the given file or a default cache file."""
        self.model.save_weights(fn or self.default_weight_filename)

    def load_weights(self, fn: str | None = None):
        """Load weights from the given file or a default cache file."""
        if not Path(fn or self.default_weight_filename).is_file():
            if fn:
                raise FileNotFoundError(f'File "{fn}" not found, cannot load weights.')
            else:
                raise RuntimeError(f'Weights data file cannot be found at the default location. Did you train the '
                                   f'model "{self.__class__.__name__}" already?')
        self.model.load_weights(fn or self.default_weight_filename)

    @property
    def default_weight_filename(self) -> str:
        """The file to save and load weights to/from when no filename is explicitly specified"""
        return config.CACHE_DIR / f'{self.__class__.__name__}.weights.h5'

    @abc.abstractmethod
    def evaluate(self):
        """Evaluate the model using appropriate methods and print the results."""
        pass

    @property
    @abc.abstractmethod
    def datasets(self) -> Tuple[tf.data.Dataset, tf.data.Dataset]:
        """Tuple of two tensorflow datasets, one for training and one for testing/evaluating"""
        pass

    @property
    @abc.abstractmethod
    def hyperparameter_tuning_objective_metric(self) -> str | keras_tuner.Objective:
        """The name of the metric that should be optimized during hyperparameter tuning"""
        pass

    def tune_hyperparameters(self):
        """Tune the hyperparameters of the model."""

        if isinstance(self.hyperparameter_tuning_objective_metric, keras_tuner.Objective):
            metric_name = (f'{self.hyperparameter_tuning_objective_metric.name}-'
                           f'{self.hyperparameter_tuning_objective_metric.direction}')
        else:
            metric_name = self.hyperparameter_tuning_objective_metric

        print(f"Tuning hyperparameters for {self.__class__.__name__}...")
        tuner = keras_tuner.Hyperband(
            self._make_model,
            objective=self.hyperparameter_tuning_objective_metric,
            max_epochs=16,
            factor=3,
            directory=config.CACHE_DIR / f'keras-tuner/{self.__class__.__name__}-{metric_name}',
            project_name=f'{self.__class__.__name__}'
        )
        train_ds, val_ds = self.datasets
        tuner.search(train_ds, validation_data=val_ds)

        # Get the optimal hyperparameters
        best_hps = tuner.get_best_hyperparameters(num_trials=1)[0]

        print(f"Tuning complete. The best hyperparameters for {self.__class__.__name__} are:\n" +
              '\n'.join(f'  - {n}: {v}' for n, v in best_hps.values.items()))
