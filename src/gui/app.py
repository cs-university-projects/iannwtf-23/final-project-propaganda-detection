from pathlib import Path

from flask import Flask, render_template, request, jsonify

from src.span_identification_st import SpanIdentifierSequenceTagging
from src.text_classification import TextClassifier

app = Flask(__name__, template_folder=Path(__file__).parent / 'templates')

si = SpanIdentifierSequenceTagging()
si.load_weights()

tc = TextClassifier()
tc.load_weights()


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/analyze', methods=['POST'])
def analyze():
    text = request.form['text']

    print("Identifying spans...")
    spans = si.predict(text, merge_conscutive_spans=True)

    print("Identifying techniques...")
    for span in spans:
        print(span)
    tc_preds = tc.predict([text[start:end] for start, end, _ in spans])

    tagged_spans = [
        {
            'start_offset': start,
            'end_offset': end,
            'confidence': confidence,
            'categories': sorted(tc_pred, key=lambda e: e[1], reverse=True)
        }
        for (start, end, confidence), tc_pred in zip(spans, tc_preds)
    ]
    return jsonify(tagged_spans)


if __name__ == '__main__':
    app.run(debug=True)
