import click


class CliGroupWithAliasSupport(click.Group):
    """Custom Cli Group for Click that supports command aliases"""

    def command(self, *args, **kwargs):
        """Adds the ability to add `aliases` to commands."""

        def decorator(f):
            aliases = kwargs.pop("aliases", None)
            if aliases and isinstance(aliases, list):
                name = kwargs.pop("name", None)
                if not name:
                    raise click.UsageError("`name` command argument is required when using aliases.")

                base_command = super(CliGroupWithAliasSupport, self).command(
                    name, *args, **kwargs
                )(f)

                for alias in aliases:
                    cmd = super(CliGroupWithAliasSupport, self).command(alias, *args, **kwargs)(f)
                    cmd.help = f"Alias for '{name}'.\n\n{cmd.help or ''}".strip()
                    cmd.short_help = f"Alias for '{name}'"
                    cmd.params = base_command.params

            else:
                cmd = super(CliGroupWithAliasSupport, self).command(*args, **kwargs)(f)

            return cmd

        return decorator
