import hashlib
import inspect
import json
from typing import List, Any

import joblib

import config

memory = joblib.Memory(config.CACHE_DIR / 'joblib', verbose=0)


def source_code_changed_validation(*args: List[Any]):
    """
    Cache key for joblib.Memory.cache to invalidate cache whenver any source
    code of the given methods changed.
    """
    filename = config.CACHE_DIR / 'source_code_hashes.json'

    def fn(metadata):
        res = True
        if filename.exists():
            stored_hashes = json.loads(filename.read_bytes())
        else:
            stored_hashes = {}
        current_hashes = {
            a.__qualname__: hashlib.sha1(inspect.getsource(a).encode()).hexdigest()
            for a in args
        }
        for k, h in current_hashes.items():
            if stored_hashes.get(k, object()) != h:  # use `object()` as a sentinel
                res = False
                break
        stored_hashes.update(current_hashes)
        filename.write_text(json.dumps(stored_hashes))
        return res

    return fn
