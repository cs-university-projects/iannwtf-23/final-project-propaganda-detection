import logging
from pathlib import Path
from typing import Dict

import pandas as pd

from src.preprocessing import eda
from src.preprocessing.load_data import iter_classified_spans


def load_classification_data(
        root: str | Path,
        augmentations_per_span: int | Dict[str, int] = 2,
) -> pd.DataFrame:
    """
    Returns a pandas DataFrame with the following columns:
     - article_id: str - the unique article id
     - start: int - the start offset of the span in the article
     - end: int - the end offset of the span in the article
     - span: str - the text content of the span
     - label: str - the propaganda technique applied in this span
    """

    root = Path(root)
    assert root.is_dir(), f"Dataset root »{root}« not found or is not a directory"

    def span_extractor(article: str, start: int, end: int):
        """Span extraction function that adds a few words of context around to one- or two-word spans."""
        if False and len(article[start:end].split()) <= 2:  # NOTE: currently context is disabled (`False and ...`)
            before, span, after = article[:start].rstrip(), article[start:end].strip(), article[end:].lstrip()
            # print(f"Span \"{span}\" with context: \"{' '.join((*before.split()[-2:], span, *after.split()[:2]))}\"")
            return ' '.join((*before.rsplit(maxsplit=2)[-2:], span, *after.split(maxsplit=2)[:2]))
        return article[start:end]

    def generator():
        for article_id, start, end, span, technique in iter_classified_spans(root, load_span_content=True, span_extractor=span_extractor):
            num_aug = augmentations_per_span[technique] if isinstance(augmentations_per_span, dict) else augmentations_per_span
            augmentations = eda.eda(span, num_aug=num_aug)
            logging.debug(f"load_data: Augmentations of \"{span}\": {augmentations}")

            assert span, f'Span is empty: "{span}" (start={start}, end={end})'

            for augmentation in augmentations:
                yield (
                    article_id,
                    start,
                    end,
                    augmentation,
                    technique,
                )

    return pd.DataFrame(generator(), columns=["article_id", "start", "end", "span", "label"])

