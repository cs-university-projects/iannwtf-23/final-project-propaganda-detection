import time

import matplotlib.pyplot as plt
import tensorflow as tf
from keras.src.callbacks import History
from tensorflow.keras import losses, optimizers, metrics

import config
from src.text_classification import TextClassifier
from src.utils import plot_training_history_graphs


class Experiments(TextClassifier):
    def __init__(self):
        super().__init__()
        self.plot_save_dir = config.CACHE_DIR / 'experiment-plots'

    def experiments(self):
        #yield 'cnn5-1-layer', tf.keras.Sequential([
        #    GloveEmbeddingLayer(mask_zero=True),
        #    layers.Conv1D(5, 3, activation='relu'),
        #    layers.Dropout(0.5),
        #    layers.GlobalMaxPooling1D(),
        #    layers.Dense(len(config.TECHNIQUES), activation='softmax')
        #])
        #yield 'bigru12-1-layer', tf.keras.Sequential([
        #    GloveEmbeddingLayer(mask_zero=True),
        #    layers.Bidirectional(layers.GRU(12, return_sequences=True)),
        #    layers.Dropout(0.5),
        #    layers.GlobalMaxPool1D(),
        #    layers.Dense(len(config.TECHNIQUES), activation='softmax')
        #])
        #yield 'bigru-2-layer', tf.keras.Sequential([
        #    GloveEmbeddingLayer(mask_zero=True),
        #    layers.Bidirectional(layers.GRU(12, return_sequences=True)),
        #    layers.Dropout(0.5),
        #    layers.Bidirectional(layers.GRU(6, return_sequences=True)),
        #    layers.Dropout(0.5),
        #    layers.GlobalMaxPool1D(),
        #    layers.Dense(len(config.TECHNIQUES), activation='softmax')
        #])
        #yield 'dense-2-layer', tf.keras.Sequential([
        #    GloveEmbeddingLayer(mask_zero=True),
        #    layers.GlobalMaxPool1D(),
        #    layers.Dense(64, activation='relu'),
        #    layers.Dropout(0.5),
        #    layers.Dense(22, activation='relu'),
        #    layers.Dropout(0.3),
        #    layers.Dense(len(config.TECHNIQUES), activation='softmax')
        #])
        #yield 'dense-1-layer', tf.keras.Sequential([
        #    GloveEmbeddingLayer(mask_zero=True),
        #    layers.GlobalMaxPool1D(),
        #    layers.Dense(64, activation='relu'),
        #    layers.Dropout(0.5),
        #    layers.Dense(len(config.TECHNIQUES), activation='softmax')
        #])
        #yield 'bert-embeddings', tf.keras.Sequential([  # NOT WORKING!
        #    tensorflow_hub.KerasLayer("https://tfhub.dev/tensorflow/bert_en_uncased_L-12_H-768_A-12/4", trainable=True),
        #    layers.Dropout(0.1),
        #    layers.Dense(len(config.TECHNIQUES), activation='softmax')
        #])
        #yield 'bilstm-attention', tf.keras.Sequential([
        #    GloveEmbeddingLayer(mask_zero=True),
        #    layers.Bidirectional(layers.LSTM(128, return_sequences=True)),
        #    layers.Attention(),  # NOT WORKING: must be called on a list of inputs
        #    layers.Dense(64, activation='relu'),
        #    layers.Dropout(0.2),
        #    layers.Dense(len(config.TECHNIQUES), activation='softmax')
        #])

        # Overfits, unstable val results:
        #inputs = tf.keras.Input(shape=(glove_embedding.SEQUENCE_LENGTH,))
        #embedding_layer = GloveEmbeddingLayer()(inputs)
        #transformer_output = TransformerEncoder(num_heads=8, ff_dim=512)(embedding_layer)
        #pooling_output = layers.GlobalAveragePooling1D()(transformer_output)
        #outputs = layers.Dense(len(config.TECHNIQUES), activation='softmax')(pooling_output)
        #yield 'transformer', tf.keras.Model(inputs=inputs, outputs=outputs), \
        #    optimizers.Adam(learning_rate=0.01)

        #yield 'cnn-flatten', tf.keras.Sequential([
        #    GloveEmbeddingLayer(mask_zero=True),
        #    layers.Conv1D(128, 5, activation='relu'),
        #    layers.MaxPooling1D(5),
        #    layers.Flatten(),
        #    layers.Dropout(0.5),
        #    layers.Dense(64, activation='relu', kernel_regularizer=tf.keras.regularizers.L2(0.01)),
        #    layers.Dropout(0.4),
        #    layers.Dense(len(config.TECHNIQUES), activation='softmax')
        #])
        #yield 'bigru-huge-2-layer', tf.keras.Sequential([
        #   GloveEmbeddingLayer(mask_zero=True),
        #   layers.Bidirectional(layers.GRU(64, return_sequences=True)),
        #   layers.Dropout(0.5),
        #   layers.Bidirectional(layers.GRU(32, return_sequences=True)),
        #   layers.Dropout(0.5),
        #   layers.GlobalMaxPool1D(),
        #   layers.Dense(32),
        #   layers.Dropout(0.5),
        #   layers.Dense(len(config.TECHNIQUES), activation='softmax')
        #])
        #yield 'cnn-huge', tf.keras.Sequential([
        #    GloveEmbeddingLayer(mask_zero=True),
        #    layers.Conv1D(32, 3, activation='relu'),
        #    layers.Dropout(0.2),
        #    layers.Conv1D(32, 3, activation='relu'),
        #    layers.MaxPooling1D(),
        #    layers.Conv1D(64, 3, activation='relu'),
        #    layers.Dropout(0.2),
        #    layers.Conv1D(64, 3, activation='relu'),
        #    layers.GlobalMaxPooling1D(),
        #    layers.Dropout(0.5),
        #    layers.Dense(len(config.TECHNIQUES), activation='softmax')
        #])

        #yield 'no-pretrained-embedding', tf.keras.Sequential([
        #    tf.keras.layers.Embedding(len(create_tokenizer().word_index), 64, mask_zero=True),
        #    tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(64, return_sequences=True)),
        #    tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(32)),
        #    tf.keras.layers.Dense(64, activation='relu'),
        #    tf.keras.layers.Dropout(0.5),
        #    layers.Dense(len(config.TECHNIQUES), activation='softmax')
        #])
        ...

    def run_all(self):
        for name, m, *args in self.experiments():
            print(f"\n\n========================================================\n"
                  f"========================================================\n"
                  f"Experiment: {name}")
            arg_optimizers = [x for x in args if isinstance(x, optimizers.Optimizer)]
            arg_losses = [x for x in args if isinstance(x, losses.Loss)]
            arg_metrics = [x for x in args if isinstance(x, metrics.Metric)]

            m.compile(
                loss=arg_losses[0] if arg_losses else losses.CategoricalCrossentropy(),
                optimizer=arg_optimizers[0] if arg_optimizers else optimizers.Adam(learning_rate=0.003),
                metrics=[
                    metrics.CategoricalAccuracy(name="accuracy"),
                    metrics.TopKCategoricalAccuracy(3, name="accuracy_top_3"),
                    *arg_metrics
                ]
            )
            self.model = m
            plt.title(name)
            self.train(epochs=22, plot=lambda *args: self.plot(name, *args))

    def plot(self, name: str, history: History):
        plt.figure(figsize=(6, 15))
        plt.title(f'Model: {name}')
        plt.tight_layout()
        p1 = plt.subplot(2, 1, 1)
        p1.title.set_text('accuracy')
        plot_training_history_graphs(history, 'accuracy')
        p2 = plt.subplot(2, 1, 2)
        p2.title.set_text('loss')
        plot_training_history_graphs(history, 'loss')
        plt.show()

        self.plot_save_dir.mkdir(parents=True, exist_ok=True)
        plt.savefig(self.plot_save_dir / f'{name}-{time.strftime("%Y-%m-%d--%H-%M-%S")}.png')


class TransformerEncoder(tf.keras.layers.Layer):
    def __init__(self, num_heads, ff_dim, dropout_rate=0.1, **kwargs):
        super(TransformerEncoder, self).__init__(**kwargs)
        self.num_heads = num_heads
        self.ff_dim = ff_dim
        self.dropout_rate = dropout_rate

    def build(self, input_shape):
        self.embedding_dim = input_shape[-1]
        self.dense_proj = tf.keras.layers.Dense(self.ff_dim, activation='relu')
        self.dense_proj_output = tf.keras.layers.Dense(self.embedding_dim)
        self.attention = tf.keras.layers.MultiHeadAttention(num_heads=self.num_heads, key_dim=self.embedding_dim)
        self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.dropout1 = tf.keras.layers.Dropout(self.dropout_rate)
        self.dropout2 = tf.keras.layers.Dropout(self.dropout_rate)
        super(TransformerEncoder, self).build(input_shape)

    @tf.function
    def call(self, inputs, training=True):
        attn_output = self.attention(inputs, inputs, training=training)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.layernorm1(inputs + attn_output, training=training)
        ffn_output = self.dense_proj(out1, training=training)
        ffn_output = self.dense_proj_output(ffn_output, training=training)
        ffn_output = self.dropout2(ffn_output, training=training)
        return self.layernorm2(out1 + ffn_output, training=training)


if __name__ == '__main__':
    Experiments().run_all()
