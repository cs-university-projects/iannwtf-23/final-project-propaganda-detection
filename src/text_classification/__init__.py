from functools import cached_property
from typing import Callable, Tuple, List, overload

import keras_tuner
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from keras.src.callbacks import History
from sklearn.metrics import classification_report
from tensorflow.keras import layers, losses, optimizers, metrics, callbacks
from tensorflow.python.data.ops.dataset_ops import AUTOTUNE

import config
from src.base_model_manager import BaseModelManager
from src.glove_embedding import GloveEmbeddingLayer, text_to_padded_sequence
from src.text_classification.preprocessing import load_classification_data
from src.utils import plot_training_history_graphs


class TextClassifier(BaseModelManager):
    """
    Classify text spans into one of many categories.
    """

    hyperparameter_tuning_objective_metric = keras_tuner.Objective("val_accuracy", direction="max")

    def _make_model(self, hp: keras_tuner.HyperParameters | None = None) -> tf.keras.Model:
        if hp:
            learning_rate = hp.Choice('learning_rate', values=[1e-2, 1e-3, 3e-3, 1e-4])
            gru_units = hp.Int('gru_units', min_value=4, max_value=32, step=8)
            dropout = hp.Choice('dropout', values=[0.1, 0.2, 0.3, 0.4, 0.5])
        else:
            learning_rate = config.TC_LEARNING_RATE
            gru_units = 12
            dropout = 0.3

        model = tf.keras.Sequential([
            tf.keras.Input((config.TC_SEQUENCE_LENGTH,)),
            # GloVe embedding layer:
            GloveEmbeddingLayer(mask_zero=False, trainable=True),

            layers.Bidirectional(layers.GRU(gru_units, return_sequences=True)),
            layers.Flatten(),
            layers.Dropout(dropout),

            # Final classification layer:
            layers.Dense(len(config.TECHNIQUES), activation='softmax',
                         kernel_regularizer=tf.keras.regularizers.L2(0.001))
        ])

        model.compile(
            loss=losses.CategoricalFocalCrossentropy(),
            optimizer=optimizers.Adam(learning_rate=learning_rate),
            metrics=[
                metrics.CategoricalAccuracy(name="accuracy"),
                metrics.TopKCategoricalAccuracy(3, name="accuracy_top_3"),
            ]
        )

        return model

    def train(self, epochs: int = 6, plot: Callable[[History], None] | bool = True):
        train_ds, val_ds = self.datasets
        train_df, val_df = self.dataframes

        print("===============================================\n"
              "Technique distribution in training dataset:",
              train_df['label'].value_counts().map(lambda c: (c, c / len(train_df))))
        print("\n\n"
              "Technique distribution in testing dataset:",
              val_df['label'].value_counts().map(lambda c: (c, c / len(val_df))))

        # We opted out of using class_weights with `CategoricalCrossEntropy` loss, since
        # `CategoricalFocalCrossEntropy` loss gave slightly better results. Uncomment
        # this and the last line in `model.fit` below to undo:
        # class_weights: pd.Series[float] = (1 / train_df['label'].value_counts()) * (len(train_df) / 2.0)

        history = self.model.fit(
            train_ds,
            batch_size=config.TC_BATCH_SIZE,
            epochs=epochs,
            validation_data=val_ds,
            callbacks=[
                # callbacks.EarlyStopping(patience=7),
                callbacks.TensorBoard(log_dir=config.TENSORBOARD_LOG_DIR / 'tc'),
            ],
            # class_weight={i: class_weights[t] for i, t in enumerate(config.TECHNIQUES)},
        )
        
        print("Training finished.")

        print(self.model.summary())

        self.evaluate()

        if callable(plot):
            plot(history)
        elif plot:
            plt.figure(figsize=(5, 12))
            plt.subplot(2, 1, 1)
            plot_training_history_graphs(history, 'accuracy')
            plt.subplot(2, 1, 2)
            plot_training_history_graphs(history, 'loss')
            plt.show()

    @overload
    def predict(self, text: str) -> List[Tuple[str, float]]: ...
    @overload
    def predict(self, text: List[str]) -> List[List[Tuple[str, float]]]: ...

    def predict(self, text: str | List[str]) -> List[Tuple[str, float]] | List[List[Tuple[str, float]]]:
        """Predict which category the given text most likely belongs to."""

        seq = text_to_padded_sequence(text, config.TC_SEQUENCE_LENGTH)
        if isinstance(text, str):
            res = self.model(seq.reshape(1, -1))[0]
            return list(zip(config.TECHNIQUES, tuple(float(x) for x in res)))
        else:
            res = self.model(seq)
            return list(
                list(zip(config.TECHNIQUES, tuple(float(x) for x in t)))
                for t in res
            )

    def evaluate(self):
        _, val_ds = self.datasets

        loss, accuracy, accuracy_top_3 = self.model.evaluate(val_ds)
        print(f'Test results: loss={loss}, accuracy={accuracy}, accuracy_top_3={accuracy_top_3}')

        x, y = tuple(zip(*val_ds))
        y_test = np.argmax(y, axis=-1).flatten()  # Convert one-hot to index, then unbatch
        y_pred = self.model.predict(np.vstack(np.array(x)))  # unbatch before passing to the model
        y_pred = np.argmax(y_pred, axis=-1)  # again, one-hot to index
        print(classification_report(y_test, y_pred, target_names=config.TECHNIQUES, zero_division=np.nan))

    @cached_property
    def datasets(self):
        print("Loading datasets...")
        train_ds = self.__create_tf_dataset(load_classification_data(config.DS_TRAIN_ROOT))
        val_ds = self.__create_tf_dataset(load_classification_data(config.DS_TEST_ROOT))

        return train_ds, val_ds

    @cached_property
    def dataframes(self):
        return (
            load_classification_data(config.DS_TRAIN_ROOT),
            load_classification_data(config.DS_TEST_ROOT)
        )

    @staticmethod
    def __create_tf_dataset(dataframe: pd.DataFrame) -> tf.data.Dataset:
        dataframe['span'] = dataframe['span'].map(lambda text: text_to_padded_sequence(text, config.TC_SEQUENCE_LENGTH))
        dataframe['label'] = dataframe['label'].map(
            lambda technique: tf.one_hot(config.TECHNIQUES.index(technique), depth=len(config.TECHNIQUES))
        )
        ds = tf.data.Dataset.from_tensor_slices(dataframe[['span', 'label']].to_dict(orient='list'))
        ds = ds.map(lambda x: (x['span'], x['label']))
        ds = ds.shuffle(250)
        ds = ds.batch(config.TC_BATCH_SIZE, drop_remainder=True)
        ds = ds.prefetch(AUTOTUNE)
        return ds
