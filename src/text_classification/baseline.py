from pathlib import Path

import numpy as np
from sklearn.metrics import classification_report

import config
from src.preprocessing.load_data import load_as_pandas_dataframe


def baseline_most_freq(ds_root: str | Path = config.DS_TEST_ROOT):
    """
    Prints a classification report for the "most-frequent" beseline,
    which just predicts the most frequent label in the dataset for all
    rows.
    """

    ds = load_as_pandas_dataframe(ds_root, load_span_content=False)
    value_counts = ds['technique'].value_counts()
    most_freq_label = value_counts.idxmax()

    y_test = ds["technique"]
    y_pred = tuple(most_freq_label for _ in y_test)

    print(f"> The most frequent label is \"{most_freq_label}\".\n")
    print(classification_report(y_test, y_pred, zero_division=np.nan))


if __name__ == '__main__':
    baseline_most_freq()
