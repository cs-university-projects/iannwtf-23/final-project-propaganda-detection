import logging
from functools import cache
from pathlib import Path
from types import MappingProxyType
from typing import List

import numpy as np
import tensorflow as tf

import config
from src.memory import memory, source_code_changed_validation
from src.preprocessing.load_data import iter_all_texts
from src.preprocessing.tokenizer import TFTreebankTokenizerWithOffsets

OOV_TOKEN = "<OOV>"  # the token to use for out-of-vocabulary items

PADDING_TYPE = 'post'
TRUNCATION_TYPE = 'post'


assert Path(config.CACHE_DIR / f'glove.6B/glove.6B.{config.EMBEDDING_OUTPUT_DIM}d.txt').is_file()


@cache
def create_tokenizer() -> TFTreebankTokenizerWithOffsets:
    print("Creating and fitting tokenizer...")
    t = TFTreebankTokenizerWithOffsets(
        num_words=config.VOCAB_SIZE,
        oov_token=OOV_TOKEN,
    )
    t.fit_on_texts(iter_all_texts())

    return t

@cache
def create_embeddings_index():
    index = {}
    with open(config.CACHE_DIR / f'glove.6B/glove.6B.300d.txt') as f:
        for line in f:
            word, *coefficients = line.split()
            index[word] = np.asarray(coefficients, dtype='float32')

    logging.debug('Found %s word vectors in embeddings index.' % len(index))

    return MappingProxyType(index)


@memory.cache(
    cache_validation_callback=source_code_changed_validation(
        create_embeddings_index,
        create_tokenizer
    ),
)
def create_word_embedding_matrix():
    tokenizer = create_tokenizer()
    not_found_words = set()

    embeddings_index = create_embeddings_index()
    matrix = np.zeros((len(tokenizer.word_index) + 1, 300))
    for word, i in tokenizer.word_index.items():
        # Note: Words not found in the embedding index will be all-zero.
        embedding_vector = embeddings_index.get(word)

        if embedding_vector is not None:
            matrix[i] = embedding_vector
        else:
            not_found_words.add(word)

    return matrix


def check_coverage():
    tokenizer = create_tokenizer()
    embeddings_index = create_embeddings_index()
    a = set()
    oov = {}
    k = 0
    i = 0
    for word, word_count in tokenizer.word_counts.items():
        if word in embeddings_index or tokenizer.clean_token(word) in embeddings_index:
            a.add(word)
            k += word_count
        else:
            oov[word] = word_count
            i += word_count

    sorted_oov = sorted(oov.items(), key=lambda e: e[1], reverse=True)

    print(f'glove_embedding: Found embeddings for {len(a) / len(tokenizer.word_counts):.2%} of vocab')
    print(f'glove_embedding: Found embeddings for  {k / (k + i):.2%} of all text')
    print(f'glove_embedding: The 25 most often occurring words not found in the embedding are: {sorted_oov[:25]}')

    return sorted_oov


def text_to_padded_sequence(text: str | List[str] | List[int] | List[List[int]], maxlen: int) -> np.ndarray:
    """Convert a string or sequence (or a list of strings or sequences) to a padded (list of) sequence(s)
    of token identifiers (integers), with the given maximum length each."""

    if not text:
        return np.array(text)

    return_single = True

    if isinstance(text, str):  # a single string
        sequences = create_tokenizer().texts_to_sequences([text])
    elif len(text) > 0 and isinstance(text[0], str):  # a list of strings
        sequences = create_tokenizer().texts_to_sequences(text)
        return_single = False
    elif len(text) > 0 and isinstance(text[0], int):  # a single sequence
        sequences = [text]
    else:  # a list of sequences
        sequences = text
        return_single = False

    res = tf.keras.utils.pad_sequences(sequences, maxlen=maxlen, padding=PADDING_TYPE,
                                       truncating=TRUNCATION_TYPE)

    return res[0] if return_single else res


class GloveEmbeddingLayer(tf.keras.layers.Embedding):
    """A glove embedding layer"""

    def __init__(self, *args, **kwargs):
        assert 'output_dim' not in kwargs

        super().__init__(
            *args,
            input_dim=len(create_tokenizer().word_index) + 1,  # + 1 because of oov token
            output_dim=config.EMBEDDING_OUTPUT_DIM,
            trainable=kwargs.pop('trainable', False),
            **kwargs,
        )

    def build(self, *args, **kwargs):
        super().build(*args, **kwargs)
        self.set_weights([create_word_embedding_matrix()])


if __name__ == '__main__':
    check_coverage()
