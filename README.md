# Propaganda Detection System

## Overview
This repository contains a propaganda detection system designed to identify instances of propaganda
techniques in news articles and other sources. The task is split into two, namely span identification,
for which there are two models (SI and SI-ST) and technique classification, which has one model (TC).

## Usage
### Command Line Interface (CLI)
- To train a model, us `./main.py train <model>`, replacing `<model>` with one of `si`, `si-st` and `tc`
- To start the graphical user interface, run `./main.py gui`
- There are other commands for evaluating model performance, running baselines, performing span 
  identification in the command line or tuning hyperparameters. Have a look at `./main.py --help`
  for details.

### Graphical User Interface (GUI)
- Run the CLI command `python launch_gui.py` to start the GUI, which provides a user-friendly interface for performing SI-ST and TC on text inputs.

## Installation
1. Clone the repository.
2. Install dependencies in a virtual environment & download data: `./setup.sh`
 
Before using, make sure to source the virtual environment: `source venv/bin/activate`.

## Models
- Span Identification (SI)
- Span Identification using Sequence Tagging (SI-ST)
- Technique Classification (TC)

## License
This project is licensed under the MIT License.
