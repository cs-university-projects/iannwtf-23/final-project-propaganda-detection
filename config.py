import os
from pathlib import Path
from typing import Tuple

from src.preprocessing.technique_map import get_all_techniques


# Dataset roots:
DS_BASE = Path(__file__).parent / 'data/'
DS_DEV_ROOT = DS_BASE / 'protechn_corpus_eval/dev'
DS_TEST_ROOT = DS_BASE / 'protechn_corpus_eval/test'
DS_TRAIN_ROOT = DS_BASE / 'protechn_corpus_eval/train'


# Log and cache directories:
LOG_DIR = Path(__file__).parent / 'logs'
TENSORBOARD_LOG_DIR = Path(__file__).parent / 'logs/tensorboard'
CACHE_DIR = Path(__file__).parent / '.cache/'
NLTK_DOWNLOAD_DIR = CACHE_DIR / 'nltk-downloads'
os.environ['NLTK_DATA'] = str(NLTK_DOWNLOAD_DIR)


EMBEDDING_OUTPUT_DIM = 300  # any length for which a file ./.cache/glove.6B/glove.6B.{LENGTH}d.txt exists is supported
"""The output dimension of the GloveEmbeddingLayer, for all models"""
VOCAB_SIZE = 35_000
"""Max. vocabulary size for the tokenizer, applies to all models"""


# SI config:
SI_BATCH_SIZE = 32
SI_SEQUENCE_LENGTH = 140  # max. text sequence length, i.e. number of tokens that each span is converted into
SI_LEARNING_RATE = 0.001  # learning rate for adam

# SI-ST config:
SI_ST_BATCH_SIZE = 15
SI_ST_SEQUENCE_LENGTH = 700  # max. text sequence length, i.e. number of tokens that the input text is chunked into
SI_ST_STEPS_PER_EPOCH = 15
SI_ST_LEARNING_RATE = 0.0001  # learning rate for adam

# TC config:
TC_BATCH_SIZE = 32
TC_SEQUENCE_LENGTH = 140  # max. text sequence length, i.e. number of tokens that each span is converted into
TC_LEARNING_RATE = 0.003  # learning rate for adam


TECHNIQUES: Tuple[str, ...] = get_all_techniques()
"""All existing technique names in a deterministic order"""


__all__ = [
    'DS_BASE', 'DS_DEV_ROOT', 'DS_TEST_ROOT', 'DS_TRAIN_ROOT', 'TECHNIQUES', 'NLTK_DOWNLOAD_DIR',
    'LOG_DIR', 'TENSORBOARD_LOG_DIR', 'CACHE_DIR', 'SI_BATCH_SIZE', 'TC_BATCH_SIZE', 'SI_ST_BATCH_SIZE',
]
