#!/bin/bash

CACHE_DIR=".cache"

python3.11 -m venv venv
source venv/bin/activate
pip install -r requirements.txt


if [ ! -d "$CACHE_DIR" ]; then
  mkdir "$CACHE_DIR"
fi


if [ ! -d "$CACHE_DIR/glove.6B" ]; then
  echo "Downloading GloVe data..."
  wget http://nlp.stanford.edu/data/glove.6B.zip -O /tmp/glove.6B.zip

  echo "Extracting..."
  unzip /tmp/glove.6B.zip -d "$CACHE_DIR/glove.6B"
else
  echo "GloVe is already downloaded."
fi

echo "Downloading wordnet nltk corpus..."
python3 -m nltk.downloader -d "$CACHE_DIR/nltk-downloads" wordnet
python3 -m nltk.downloader -d "$CACHE_DIR/nltk-downloads" punkt



