#!/bin/env python3
import logging
import os
from io import TextIOWrapper
from pathlib import Path
from threading import Timer
from typing import Literal

import click

from src.cli_group_with_alias_support import CliGroupWithAliasSupport


@click.group(cls=CliGroupWithAliasSupport)
@click.option('-v', '--verbose', is_flag=True, default=False)
def cli(verbose: bool):
    import config  # using local imports where possible to only load what's necessary for the command being run

    # Call setup script if cache does not yet exist:
    if not config.CACHE_DIR.is_dir() or len(tuple(config.CACHE_DIR.iterdir())) == 0:
        os.system(f"bash \"{Path(__file__).parent / 'setup.sh'}\"")

    if verbose:
        logging.basicConfig(level=logging.DEBUG)


@cli.command(name='gui', help="Launch the graphical user interface.")
@click.option('--debug', is_flag=True, default=False, help="Start the graphical user interface in debug mode.")
@click.option('--port', default=3374, help="Specify the port to run the graphical user interface on.")
def gui(debug: bool, port: int):
    import webbrowser
    from src.gui.app import app

    # Schedule opening the app in the browser:
    Timer(0.5, webbrowser.open, (f"http://127.0.0.1:{port}",)).start()

    # Run the app:
    app.run(debug=debug, port=port)


@cli.command(name='train', help="Train a model and save it's weights afterwards.")
@click.argument('model', type=click.Choice(['si', 'si-st', 'tc']))
@click.option('--epochs', default=6, type=click.INT, help='Number of epochs.')
@click.option('--dont-save', is_flag=True, default=False, help="Do not save the model's weights after training")
@click.option('--weights-file', default=None, type=click.Path(),
              help='Path to the file to save model weights in after training.')
def train(model: Literal['si', 'si-st', 'tc'], epochs: int, dont_save: bool, weights_file: str | None):
    # Create a model of the appropriate type:
    if model == 'si':
        from src.span_identification import SpanIdentifier
        model = SpanIdentifier()
    elif model == 'si-st':
        from src.span_identification_st import SpanIdentifierSequenceTagging
        model = SpanIdentifierSequenceTagging()
    else:
        from src.text_classification import TextClassifier
        model = TextClassifier()

    # Train the model:
    model.train(epochs=epochs)

    # Save weights, if not opted out:
    if not dont_save:
        model.save_weights(weights_file)


@cli.command(name='evaluate', help="Evaluate the performance of a (trained) model.")
@click.argument('model', type=click.Choice(['si', 'si-st', 'tc']))
@click.option('--weights-file', default=None, type=click.Path(exists=True),
              help='Path to the file to load the model weights from.')
def evaluate(model: Literal['si', 'si-st', 'tc'], weights_file: str | None):
    # Create a model of the appropriate type:
    if model == 'si':
        from src.span_identification import SpanIdentifier
        model = SpanIdentifier()
    elif model == 'si-st':
        from src.span_identification_st import SpanIdentifierSequenceTagging
        model = SpanIdentifierSequenceTagging()
    else:
        from src.text_classification import TextClassifier
        model = TextClassifier()

    # Load the model's weights:
    model.load_weights(weights_file)

    # Evaluate the model:
    model.evaluate()


@cli.command(name='run-baseline', help="Run a baseline for a model.")
@click.argument('model', type=click.Choice(['si', 'si-st', 'tc']))
def run_baseline(model: Literal['si', 'si-st', 'tc']):
    if model == 'si':
        from src.span_identification.baseline import baseline_logistic_regression as baseline
    elif model == 'si-st':
        from src.span_identification_st.baseline import baseline_all_nonprop as baseline
    else:
        from src.text_classification.baseline import baseline_most_freq as baseline

    if hasattr(baseline, '__doc__'):
        print("============================\n"
              f"Baseline Description: {baseline.__doc__}")

    baseline()


@cli.command(name='tune-hp', help="Tune the hyperparameters of a model.")
@click.argument('model', type=click.Choice(['si', 'si-st', 'tc']))
def tune_hp(model: Literal['si', 'si-st', 'tc']):
    # Create the appropriate model:
    if model == 'si':
        from src.span_identification_st import SpanIdentifierSequenceTagging
        model = SpanIdentifierSequenceTagging()
    elif model == 'si-st':
        from src.span_identification import SpanIdentifier
        model = SpanIdentifier()
    else:
        from src.text_classification import TextClassifier
        model = TextClassifier()

    # Tune hyperparameters:
    model.tune_hyperparameters()


@cli.command(name='span-identification-sequence-tagging', aliases=['si-st'],
             help="Run span identification on the given text file.")
@click.argument('input_file', type=click.File())
def span_identification_st(input_file: TextIOWrapper):
    from src.span_identification_st import SpanIdentifierSequenceTagging
    from src.span_identification_st.utils import print_with_highlighted_spans

    content = input_file.read()

    span_identifier = SpanIdentifierSequenceTagging()
    span_identifier.load_weights()
    spans = span_identifier.predict(content, merge_conscutive_spans=False)

    print_with_highlighted_spans(content, spans)


if __name__ == '__main__':
    cli()
